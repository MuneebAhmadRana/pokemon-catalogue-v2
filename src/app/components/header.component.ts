import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { SessionService } from "../shared/session.service";

@Component({
    styleUrls: ['./header.component.css'],
    templateUrl: './header.component.html',
    selector: 'app-header',
})
export class HeaderComponent {

    constructor(private readonly sessionService: SessionService,
        private readonly router: Router,
        ) {}

    public onLogoutClicked() {
        this.sessionService.removeSession(); 
        this.router.navigateByUrl('/login')
    }
}