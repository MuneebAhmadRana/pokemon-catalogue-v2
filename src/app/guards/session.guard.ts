import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { LoginAPI } from '../login/api/login.api';
import { LoginFacade } from '../login/login.facade';
import { LoginState } from '../login/state/login.state';
import { SessionService } from '../shared/session.service';

@Injectable({
  providedIn: 'root',
})
export class SessionGuard implements CanActivate {
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (this.sessionService.isActive()) {
      this.loginFacade.fetchUser(this.sessionService.getSession().trainerName);
      return true;
    } else {
      this.router.navigateByUrl('/login');
      return false;
    }
  }

  constructor(
    private readonly router: Router,
    private readonly sessionService: SessionService,
    private readonly loginFacade: LoginFacade
  ) {}
}
