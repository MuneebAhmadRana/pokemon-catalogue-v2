import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { mergeMap, tap } from 'rxjs/operators';
import { User } from 'src/app/models/user.model';
import { SessionService } from 'src/app/shared/session.service';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class LoginAPI {
    private user$: BehaviorSubject<User> = new BehaviorSubject(null);
    baseURL = environment.baseDatabaseApiUrl
    constructor(private readonly http: HttpClient) {

    }

    public login(username: string): Observable<User> {
        const login$ = this.http.get<User[]>(`${this.baseURL}users/?trainerName=${username}`);
        const register$ = this.http.post<User>(`${this.baseURL}users`, {
            trainerName: username,
            pokemon: [],
        });

        return login$.pipe(
            mergeMap((loginResponse: User[]) => {
                const user = loginResponse.pop();
                if (!user) {
                    return register$;
                } else {
                    return of(user);
                }
            })
        )
    }

    public fetchUserByName(username: string): Observable<User[]> {
        return this.http.get<User[]>(`${this.baseURL}users/?trainerName=${username}`);
    }
}