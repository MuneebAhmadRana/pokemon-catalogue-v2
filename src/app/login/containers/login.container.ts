import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from 'src/app/shared/session.service';
import { LoginFacade } from '../login.facade';

@Component({
  selector: 'app-login',
  styleUrls: ['./login.container.css'],
  templateUrl: './login.container.html',
})
export class LoginContainer implements OnInit {
  userFormGroup = new FormGroup({
    username: new FormControl('', [
      Validators.minLength(2),
      Validators.required,
    ]),
  });

  constructor(
    private readonly loginFacade: LoginFacade,
    private readonly sessionService: SessionService,
    private readonly router: Router,
  ) { }

  onLoginClicked() {
    this.loginFacade.logIn(this.username.value);
  }

  get username(): AbstractControl {
    return this.userFormGroup.get('username');
  }

  get isLoading(): Observable<boolean> {
    return this.loginFacade.isLoading$;
  }

  getError() {
    if (this.username.hasError('required')) {
      return 'You must enter you trainer name';
    } else if (this.username.hasError('minlength')) {
      return 'Must be longer than 1';
    } else return '';
  }

  ngOnInit() {
    if (this.sessionService.isActive()) {
      this.router.navigateByUrl('/pokemon');
    }
  }
}
