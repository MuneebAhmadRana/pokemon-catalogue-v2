import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import { SessionService } from '../shared/session.service';
import { LoginAPI } from './api/login.api';
import { LoginState } from './state/login.state';

@Injectable({
  providedIn: 'root',
})
export class LoginFacade {
  constructor(
    private readonly loginAPI: LoginAPI,
    private readonly loginState: LoginState,
    private readonly router: Router,
    private readonly sessionService: SessionService
  ) {}

  public get user$(): Observable<User> {
    return this.loginState.user$;
  }

  public setUser(user: User): void {
    this.loginState.setUser(user);
  }

  public setState(isLoading: boolean): void {
    this.loginState.setIsLoading(isLoading);
  }

  public get isLoading$(): Observable<boolean> {
    return this.loginState.isLoading$;
  }

  public logIn(username: string): void {
    this.setState(true);
    this.loginAPI.login(username).subscribe(
      (user: User) => {
        this.setUser(user);
        this.sessionService.setSession(user);
        this.setState(false);
        this.router.navigateByUrl('/pokemon/trainer');
      }
    );
  }

  public fetchUser(username: string): void {
    this.loginAPI.fetchUserByName(username).subscribe((users: User[]) => {
      let user = users.pop();
      this.loginState.setUser(user);
    });
  }

  public get userID() {
    return this.loginState.userID;
  }
}
