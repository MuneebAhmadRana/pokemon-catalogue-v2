import { NgModule } from '@angular/core';
import { LoginContainer } from './containers/login.container'
import { ReactiveFormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from "@angular/material/list";
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        LoginContainer,
    ],
    imports: [
        MatProgressSpinnerModule,
        ReactiveFormsModule,
        MatListModule,
        MatFormFieldModule,
        MatInputModule,
        MatCardModule,
        MatButtonModule,
        CommonModule,
    ],
    exports: [

    ],
    bootstrap: [LoginContainer],
})
export class LoginModule { }