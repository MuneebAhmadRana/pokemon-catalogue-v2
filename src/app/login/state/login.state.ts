import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { LoginAPI } from '../api/login.api';

@Injectable({
    providedIn: 'root'
})
export class LoginState {
    private _user$: BehaviorSubject<User> = new BehaviorSubject(null);
    private _isLoading$: BehaviorSubject<boolean> = new BehaviorSubject(false);
    constructor() { }

    public get user$ (): Observable<User> {
        return this._user$.asObservable();
    }

    public setUser (user: User): void {
        this._user$.next(user);
    }

    public setIsLoading (isLoading: boolean): void {
        this._isLoading$.next(isLoading);
    }

    public get isLoading$ (): Observable<boolean> {
        return this._isLoading$.asObservable();
    }

    public get userID(): number {
        return this._user$.value.id;
    }
}