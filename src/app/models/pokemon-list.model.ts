// To parse this data:
//
//   import { Convert, PokemonList } from "./file";
//
//   const pokemonList = Convert.toPokemonList(json);

export interface PokemonList {
    count: number;
    next: string;
    previous: string;
    results: PokemonListItem[];
}

export interface PokemonListItem {
    id?: number;
    name: string;
    url: string;
}
