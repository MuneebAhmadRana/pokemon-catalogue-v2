export interface User {
    id?: number;
    trainerName: string;
    pokemon: Array<number>;
}