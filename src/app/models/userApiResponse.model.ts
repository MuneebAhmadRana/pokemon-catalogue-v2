import { User } from './user.model';

export interface UserAPIResponse extends Array<User> { }