import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { PokemonList, PokemonListItem } from 'src/app/models/pokemon-list.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PokemonCatalogueAPI {
  private _pokemonList$: BehaviorSubject<
    PokemonListItem[]
  > = new BehaviorSubject([]);
  baseUrl = environment.basePokemonApiUrl;
  prev: string = null;
  next: string = null;
  count: number = 0;

  constructor(private readonly http: HttpClient) {
    this.fetchPokemonList();
  }

  getPokemonList(): Observable<any> {
    return this._pokemonList$.asObservable();
  }

  fetchPokemonList(): Observable<PokemonList> {
    return this.http
      .get<PokemonList>(`${this.baseUrl}pokemon/?limit=15`);
  }

  fetchPokemonListByUrl(url: string): Observable<PokemonList> {
    return this.http.get<PokemonList>(url);
  }

  resetPokemonList(): void {
    this.fetchPokemonList();
  }

  getCount(): number {
    return this.count;
  }
}