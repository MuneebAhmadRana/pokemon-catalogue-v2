import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Pokemon } from 'src/app/models/pokemon.model';
import { environment } from 'src/environments/environment';
const { basePokemonApiUrl } = environment
@Injectable({
    providedIn: "root"
})
export class PokemonDetailAPI {
    constructor(private readonly http: HttpClient) { }

    public pokemon: BehaviorSubject<Pokemon> = new BehaviorSubject(null);

    // Getter that will be used in the facade. 
    public getPokemon(): Observable<Pokemon> {
        return this.pokemon.asObservable();
    }

    // Fetches pokemon from API
    public fetchPokemonByName(name: string): Observable<Pokemon> {
        return this.http.get<Pokemon>(`${basePokemonApiUrl}pokemon/${name}`)
    }
}