import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from "../../../environments/environment";
import { LoginState } from 'src/app/login/state/login.state';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';

const { basePokemonApiUrl, baseDatabaseApiUrl } = environment;


@Injectable({
    providedIn: "root"
})
export class PokemonTrainerAPI {
    constructor(private readonly http: HttpClient, private readonly loginState: LoginState) {
    }

    fetchPokemon(id: number): Observable<Pokemon> {
        return this.http.get<Pokemon>(`${basePokemonApiUrl}pokemon/${id}`);
    }

    updatePokemons(pokemons: number[], trainerID: number): Observable<User> {       /* this.loginState.user$.subscribe(userdb => user = userdb); */
        if (trainerID) {
            return this.http.patch<User>(`${baseDatabaseApiUrl}users/${trainerID}`, {
                pokemon: pokemons
            });
        } else {
            return null;
        }
    }

}