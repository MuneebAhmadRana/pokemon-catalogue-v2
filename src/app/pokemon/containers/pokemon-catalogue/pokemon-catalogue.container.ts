import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator/paginator';
import { Router } from '@angular/router';
import { PokemonFacade } from '../../pokemon.facade';

@Component({
  selector: 'app-catalogue',
  templateUrl: './pokemon-catalogue.container.html',
  styleUrls: ['./pokemon-catalogue.container.css'],
})
export class PokemonCatalogueContainer implements OnInit {
  paginatorIndex: number = 0;
  pokemonCount: number = 0;

  getIdAndImage(url: string): { id: string; url: string } {
    const id = url.split('/').filter(Boolean).pop();
    return {
      id,
      url: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`,
    };
  }

  onClicked(pokemonName: number): void {
    this.router.navigate(['detail/' + pokemonName]);
  }

  get pokemonList$() {
    this.pokemonCount = this.pokemonFacade.catalogueListCount;
    return this.pokemonFacade.pokemonCatalogueList;
  }

  onPaginatorChanged(event: PageEvent) {
    const { pageIndex } = event;
    if (pageIndex > this.paginatorIndex) {
      this.paginatorIndex = pageIndex;
      this.pokemonFacade.pokemonCatalogueListNext();
    } else {
      this.paginatorIndex = pageIndex;
      this.pokemonFacade.pokemonCatalogueListPrev();
    }
    window.scroll(0, 0);
  }

  constructor(
    private router: Router,
    private pokemonFacade: PokemonFacade
  ) { }

  ngOnInit(): void { }
}
