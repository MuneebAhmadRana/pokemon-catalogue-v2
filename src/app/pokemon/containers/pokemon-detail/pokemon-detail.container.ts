import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Pokemon } from 'src/app/models/pokemon.model';
import { isBuffer } from 'util';
import { PokemonDetailAPI } from '../../api/pokemon-detail.api';
import { PokemonFacade } from '../../pokemon.facade';

@Component({
  selector: 'app-detail',
  templateUrl: './pokemon-detail.container.html',
  styleUrls: ['./pokemon-detail.container.css'],
})
export class PokemonDetailContainer implements OnInit {
  private readonly pokemonName: string = "";
  constructor(private readonly route: ActivatedRoute, private readonly pokemonFacade: PokemonFacade) {
    this.pokemonName = this.route.snapshot.paramMap.get("name");
  }

  get pokemon$(): Observable<Pokemon> {
    return this.pokemonFacade.getPokemonDetailPokemon();
  }

  get trainersPokemons(): number[] {
    return this.pokemonFacade.getTrainersPokemonsList();
  }

  get pokemonExists(): boolean {
    let pokemon: Pokemon = null;
    this.pokemon$.subscribe(pok => {
      pokemon = pok;
    });
    if (this.trainersPokemons.includes(pokemon.id)) {
      return true;
    }
    else {
      return false;
    }
  }

  addPokemonToCollection(id: number) {
    this.trainersPokemons.push(id);
    this.pokemonFacade.updateTrainersPokemons(this.trainersPokemons);
  }

  removePokemonFromCollection(id: number) {
    const index: number = this.trainersPokemons.indexOf(id);
    this.trainersPokemons.splice(index);

    this.pokemonFacade.updateTrainersPokemons(this.trainersPokemons);
  }
  ngOnInit(): void {
    /* this.pokemonFacade.subscribe(); */
    this.pokemonFacade.setPokemonDetailPokemon(this.pokemonName);
  }
}
