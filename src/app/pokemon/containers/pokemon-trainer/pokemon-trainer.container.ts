import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginState } from 'src/app/login/state/login.state';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { PokemonFacade } from '../../pokemon.facade';

@Component({
    selector: "app-trainer-page",
    templateUrl: "./pokemon-trainer.container.html",
    styleUrls: ["./pokemon-trainer.container.css"]
})
export class PokemonTrainerContainer implements OnInit {
    constructor(private readonly loginState: LoginState, private readonly pokemonFacade: PokemonFacade, private readonly router: Router) {

    }

    caughtPokemon: number[];

    get Trainer$(): Observable<User> {
        return this.pokemonFacade.user$;
    }

    get Pokemons$(): Observable<Pokemon[]> {
        return this.pokemonFacade.TrainerPokemons;
    }

    onPokemonClick(name: string) {
        this.router.navigateByUrl(`/pokemon/detail/${name}`)
    }
    ngOnInit(): void {
        this.pokemonFacade.setTrainerPokemons();
    }
}