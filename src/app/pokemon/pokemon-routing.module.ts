import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from '../components/not-found/not-found.component';
import { SessionGuard } from '../guards/session.guard';
import { PokemonCatalogueContainer } from './containers/pokemon-catalogue/pokemon-catalogue.container';
import { PokemonDetailContainer } from './containers/pokemon-detail/pokemon-detail.container';
import { PokemonTrainerContainer } from './containers/pokemon-trainer/pokemon-trainer.container';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'trainer',
    },
    {
        path: 'trainer',
        component: PokemonTrainerContainer,
        canActivate: [SessionGuard],
    },
    {
        path: 'catalogue',
        component: PokemonCatalogueContainer,
        canActivate: [SessionGuard],
    },
    {
        path: 'detail/:name',
        component: PokemonDetailContainer,
        canActivate: [SessionGuard],
    },
    {
        path: '**',
        component: NotFoundComponent,
        canActivate: [SessionGuard],
    }
]
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PokemonRouting {

}