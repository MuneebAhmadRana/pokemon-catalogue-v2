import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginAPI } from '../login/api/login.api';
import { LoginFacade } from '../login/login.facade';
import { LoginState } from '../login/state/login.state';
import { PokemonList, PokemonListItem } from '../models/pokemon-list.model';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { PokemonCatalogueAPI } from './api/pokemon-catalogue.api';
import { PokemonDetailAPI } from './api/pokemon-detail.api';
import { PokemonTrainerAPI } from './api/pokemon-trainer.api';
import { PokemonCatalogueState } from './state/pokemon-catalogue.state';
import { PokemonDetailState } from './state/pokemon-detail.state';
import { PokemonTrainerState } from './state/pokemon-trainer.state';

@Injectable({
  providedIn: 'root',
})
export class PokemonFacade {
  constructor(
    private readonly pokemonDetailAPI: PokemonDetailAPI,
    private readonly pokemonDetailState: PokemonDetailState,
    private readonly pokemonCatalogueState: PokemonCatalogueState,
    private readonly pokemonCatalogueAPI: PokemonCatalogueAPI,
    private readonly pokemonTrainerState: PokemonTrainerState,
    private readonly pokemonTrainerAPI: PokemonTrainerAPI,
    private readonly loginState: LoginState,
    private readonly loginFacade: LoginFacade,
  ) {
    this.pokemonCatalogueAPI.fetchPokemonList().subscribe((list: PokemonList) => {
      this.pokemonCatalogueState.setNext(list.next);
      this.pokemonCatalogueState.setPrev(list.previous);
      this.pokemonCatalogueState.setNewList(list.results);
      this.pokemonCatalogueState.setCount(list.count);
    });
  }

  public get user$() {
    return this.loginFacade.user$;
  }

  public get pokemonCatalogueList(): Observable<PokemonListItem[]> {
    return this.pokemonCatalogueState.pokemonList$;
  }

  public pokemonCatalogueListNext(): void {
    this.pokemonCatalogueAPI
      .fetchPokemonListByUrl(this.pokemonCatalogueState.nextPage)
      .subscribe((list: PokemonList) => {
        this.pokemonCatalogueState.setNext(list.next);
        this.pokemonCatalogueState.setPrev(list.previous);
        this.pokemonCatalogueState.setNewList(list.results);
      });
  }

  public pokemonCatalogueListPrev(): void {
    this.pokemonCatalogueAPI
      .fetchPokemonListByUrl(this.pokemonCatalogueState.prevPage)
      .subscribe((list: PokemonList) => {
        this.pokemonCatalogueState.setNext(list.next);
        this.pokemonCatalogueState.setPrev(list.previous);
        this.pokemonCatalogueState.setNewList(list.results);
      });
  }

  public get catalogueListCount(): number {
    return this.pokemonCatalogueState.listEntryCount;
  }

  public setPokemonCatalogueList(pokemonList: PokemonListItem[]) {
    this.pokemonCatalogueState.setNewList(pokemonList);
  }

  //POKEMON DETAIL PART
  public getPokemonDetailPokemon(): Observable<Pokemon> {
    return this.pokemonDetailState.getPokemon$();
  }

  public setPokemonDetailPokemon(pokemon: string): void {
    this.pokemonDetailAPI.fetchPokemonByName(pokemon).subscribe((pokemon: Pokemon) => {
      this.pokemonDetailState.setPokemon$(pokemon);
    })
  }


  //POKEMON TRAINER PART
  public get TrainerPokemons(): Observable<Pokemon[]> {
    return this.pokemonTrainerState.getTrainerPokemons();
  }

  public setTrainerPokemons(): void {
    this.user$.subscribe((trainer) => {
      if (trainer) {
        let pokemonList: Pokemon[] = [];
        trainer.pokemon.forEach(id => {
          this.pokemonTrainerAPI.fetchPokemon(id).subscribe(pokemon => {
            pokemonList.push(pokemon);
          });
          this.pokemonTrainerState.setTrainerPokemons(pokemonList);
        })
      }
    })
  }

  getTrainersPokemonsList(): number[] {
    let pokemonList: number[] = [];
    this.loginState.user$.subscribe(user => {
      pokemonList = user.pokemon;
    });
    return pokemonList;
  }

  updateTrainersPokemons(pokemons: number[]) {
    this.pokemonTrainerAPI.updatePokemons(pokemons, this.loginFacade.userID).subscribe(user => {
      this.loginFacade.setUser(user);
    })
  }
}
