import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { PokemonCatalogueContainer } from './containers/pokemon-catalogue/pokemon-catalogue.container';
import { PokemonRouting } from './pokemon-routing.module';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import { CommonModule } from '@angular/common';
import { PokemonDetailContainer } from './containers/pokemon-detail/pokemon-detail.container';
import { PokemonTrainerContainer } from './containers/pokemon-trainer/pokemon-trainer.container';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    declarations: [PokemonCatalogueContainer, PokemonDetailContainer, PokemonTrainerContainer],
    imports: [
        PokemonRouting,
        MatButtonModule,
        MatPaginatorModule,
        MatCardModule,
        CommonModule,
        NgbModule
    ],
    exports: []
})
export class PokemonModule {

}