import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { PokemonList, PokemonListItem } from "src/app/models/pokemon-list.model";

@Injectable({
    providedIn: 'root'
})
export class PokemonCatalogueState {
    // TODO: Add caching
    private _pokemonList$: BehaviorSubject<PokemonListItem[]> = new BehaviorSubject([]);
    private next: string = null;
    private prev: string = null;
    private count: number = 0;

    constructor() { }

    public setNext(url: string): void {
        this.next = url;
    }

    public setPrev(url: string): void {
        this.prev = url;
    }

    public setCount(count: number): void {
        this.count = count;
    }

    public get nextPage(): string {
        return this.next;
    }

    public get prevPage(): string {
        return this.prev;
    }

    public get listEntryCount(): number {
        return this.count;
    }

    public setNewList(pokemonList: PokemonListItem[]): void {
        this._pokemonList$.next(pokemonList);
    }

    public get pokemonList$(): Observable<PokemonListItem[]> {
        return this._pokemonList$.asObservable();
    }
}