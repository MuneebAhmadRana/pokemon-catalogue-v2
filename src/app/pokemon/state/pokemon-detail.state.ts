import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Pokemon } from 'src/app/models/pokemon.model';

@Injectable({
    providedIn: "root"
})
export class PokemonDetailState {

    private _pokemon$: BehaviorSubject<Pokemon> = new BehaviorSubject(null);

    public getPokemon$(): Observable<Pokemon> {
        return this._pokemon$.asObservable();
    }
    public setPokemon$(pokemon: Pokemon): void {
        this._pokemon$.next(pokemon);
    }
}