import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';

@Injectable({
    providedIn: "root"
})
export class PokemonTrainerState {
    private _trainersPokemons$: BehaviorSubject<Pokemon[]> = new BehaviorSubject(null);

    public getTrainerPokemons() {
        return this._trainersPokemons$.asObservable();
    }

    public setTrainerPokemons(pokemon: Pokemon[]) {
        this._trainersPokemons$.next(pokemon);
    }
}