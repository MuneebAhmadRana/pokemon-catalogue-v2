import { Injectable } from '@angular/core';
import { STORAGE_KEYS } from '../enums/storage-keys.enum';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  constructor() {}

  setSession(trainer: User) {
    delete trainer.id;
    localStorage.setItem(STORAGE_KEYS.TRAINER, btoa(JSON.stringify(trainer)));
  }

  getSession(): User {
    const encodedUser = localStorage.getItem(STORAGE_KEYS.TRAINER);
    if (encodedUser) {
        return JSON.parse(atob(encodedUser));
    } else {
        return null;
    }
  }

  isActive(): boolean {
    const trainer = this.getSession();
    return Boolean(trainer);
  }

  removeSession(): void {
    localStorage.removeItem(STORAGE_KEYS.TRAINER);
  }
}
